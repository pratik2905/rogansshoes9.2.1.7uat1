var Constant;
(function (Constant) {
    Constant.GET = "GET";
    Constant.json = "json";
    Constant.Function = "function";
    Constant.string = "string";
    Constant.object = "object";
    Constant.innerLoderHtml = "<div class='loader-inner' style='margin:0 auto;text-align:center;padding:20px;'><img src='../Content/Images/throbber.gif' alt='Loading' class='dashboard-loader' /></div>";
    Constant.configurableProduct = "ConfigurableProduct";
    Constant.simpleProduct = "SimpleProduct";
    Constant.groupedProduct = "GroupedProduct";
    Constant.bundleProduct = "BundleProduct";
    Constant.addOns = "AddOns";
    Constant.ZnodeCustomerShipping = "ZnodeCustomerShipping";
    Constant.GuestUser = "Guest User";
    Constant.gocoderGoogleAPI = $("#gocoderGoogleAPI").val(); //To be fetched from config file
    Constant.gocoderGoogleAPIKey = $("#gocoderGoogleAPIKey").val(); //To be fetched from config file 
    Constant.inventory = "Inventory";
    Constant.category = "Category";
    Constant.seo = "SEO";
    Constant.defaultAdmin = "admin@znode.com";
    Constant.CATALOG = "Catalog";
    Constant.image = "Image";
    Constant.AmericanExpressCardCode = "AMEX";
    Constant.shippingSettings = "ShippingSettings";
    Constant.productSetting = "ProductSetting";
    Constant.productDetails = "ProductDetails";
    Constant.storelist = "Storelist";
})(Constant || (Constant = {}));
var ErrorMsg;
(function (ErrorMsg) {
    ErrorMsg.CallbackFunction = "Callback is not defined. No request made.";
    ErrorMsg.APIEndpoint = "API Endpoint not available: ";
    ErrorMsg.InvalidFunction = "invalid function name : ";
    ErrorMsg.ErrorMessageForCategoryCode = "Alphanumeric values are allowed,Must contain at least one alphabet in CategoryCode.";
})(ErrorMsg || (ErrorMsg = {}));
//# sourceMappingURL=Znode.Model.js.map