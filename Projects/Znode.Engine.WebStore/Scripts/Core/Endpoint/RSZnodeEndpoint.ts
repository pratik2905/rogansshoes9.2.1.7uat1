﻿class RSZnodeEndpoint extends ZnodeBase {
    
    constructor() {
        super();
    }

    /*Nivi code Start*/
    GetMapURLSuccess(state, sku, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }
    GetStoresForStoreLocatorSuccess(state, sku, _userCord, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }
    SaveinCookieSuccess(selectedstoreid, storename, storeaddress, callbackMethod) {
        //alert("SaveinCookieSuccess " + storename + ", " + storeaddress + ", " + selectedstoreid);
        super.ajaxRequest("/rshome/saveincookie/", Constant.Post, { "storeId": selectedstoreid, "storeName": storename, "storeAddress": storeaddress }, callbackMethod, "json");
    }
    RemoveStoreDetailsForCartOnShipClickSuccess(productId, callbackMethod) {
        super.ajaxRequest("/rscart/UpdateDeliveryPreferenceToShip/", Constant.Post, { "productId": productId }, callbackMethod, "json");
    }
    AssignStoreDetailsForCartSuccess(selectedstoreid, storename, storeaddress, productId, callbackMethod) {
       // alert("selectedstoreid endpoint" + selectedstoreid)
        super.ajaxRequest("/rscart/UpdateDeliveryPreferenceToPickUp/", Constant.Post, { "storeId": selectedstoreid, "storeName": storename, "storeAddress": storeaddress, "productId": productId }, callbackMethod, "json");
    }
    ClickCartSuccess(selectedstoreid, storename, storeaddress, productId, callbackMethod) {
        super.ajaxRequest("/rscart/UpdateDeliveryPreferenceToPickUp/", Constant.Post, { "storeId": selectedstoreid, "storeName": storename, "storeAddress": storeaddress, "productId": productId }, callbackMethod, "json");
    }
    GetStoresForCartSuccess(state, sku, _userCord, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }
    GetStockForSelectedStoreSuccess(state, sku, selectedstore, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }
    GetStoresSuccess(state, sku, _userCord, callbackMethod) {
        //   alert("endpoint");
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }
    GetStockForSelectedStoreClickCartSuccess(state, sku, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }
    GetStockForSelectedStoreCartSuccess(state, sku, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }
    GetStockForCheckoutSuccess(state, sku, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }

    GetCurrentSelectedStoreDetailsSuccess(state, sku, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/" + state + "/" + sku, Constant.GET, { "state": state, "sku": sku }, callbackMethod, "json");
    }

    FillLocationDropdownsuccess(sku, callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/rspublishproduct/GetStoreLocationsDetails/ALL/" + sku, Constant.GET, { "sku": sku }, callbackMethod, "json");
    }

    FillBrandsDropdownSuccess(callbackMethod) {
        super.ajaxRequest($("#apiURL").val() + "/Brand/List", Constant.GET, {}, callbackMethod, "json");
    }

    /*Nivi code End*/


}