﻿var bLazy: any;
class Home extends ZnodeBase {
    constructor() {
        super();
    }
    Init() {
        $(document).ready(function () {
            $(".product-list-widget .owl-next").off("click");
            $(".product-list-widget .owl-next").on("click", Home.prototype.loadImages);
        });
    }

    loadImages(): void {
        var productParent = $(this).parentsUntil('.product-list-widget');
        var unloadedElements = productParent ? productParent.find(".b-lazy:not(.b-loaded)") : null;
        if (unloadedElements && unloadedElements.length > 0)
            bLazy.load($(unloadedElements));
    }
    

    /*Orignal code Start: validation for email id*/
    //ValidationForEmailID(): any {
    //    $("#newslettererrormessage").removeClass();
    //    var signUpEmail: string = $("#txtNewsLetterSignUp").val();
    //    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    //    if (signUpEmail != null && signUpEmail != "") {
    //        if (!pattern.test(signUpEmail)) {
    //            $("#newslettererrormessage").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"))
    //            $("#newslettererrormessage").addClass("error-msg");
    //            $("#newslettererrormessage").show();
    //            return false;
    //        }
    //        else {
    //            $("#newslettererrormessage").html('');
    //            $("#newslettererrormessage").removeClass("error-msg");
    //            $("#newslettererrormessage").hide();
    //            Endpoint.prototype.SignUpForNewsLetter(signUpEmail, function (response) {
    //                if (response.sucess) {
    //                    $("#txtNewsLetterSignUp").val('');
    //                    $("#newslettererrormessage").addClass("success-msg");
    //                    $("#newslettererrormessage").show().html(response.message);
    //                } else {
    //                    $("#newslettererrormessage").addClass("error-msg");
    //                    $("#newslettererrormessage").show().html(response.message);
    //                }
    //            });
    //        }
    //    }
    //    else {
    //        $("#newslettererrormessage").html(ZnodeBase.prototype.getResourceByKeyName("RequiredEmailId"))
    //        $("#newslettererrormessage").addClass("error-msg");
    //        $("#newslettererrormessage").show();
    //        return false;
    //    }
    //}
    /*Orignal code End: validation for email id*/

    /*Nivi code start*/
    ValidationForEmailID(): any {
        $("#newslettererrormessage").removeClass();
        var signUpEmail: string = $("#txtNewsLetterSignUp").val();
        var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (signUpEmail != null && signUpEmail != "") {
            if (!pattern.test(signUpEmail)) {
                $("#newslettererrormessage").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"))
                $("#newslettererrormessage").addClass("error-msg");
                $("#newslettererrormessage").show();
                return false;
            }
            else {
                $("#newslettererrormessage").html('');
                $("#newslettererrormessage").removeClass("error-msg");
                $("#newslettererrormessage").hide();
                Endpoint.prototype.SignUpForNewsLetter(signUpEmail, function (response) {
                    if (response.sucess) {
                        $("#txtNewsLetterSignUp").val('');
                        $("#newslettererrormessage2").addClass("success-msg");
                        $("#newslettererrormessage2").show().html(response.message);
                        $("#newslettererrormessage").hide();
                    } else {
                        $("#newslettererrormessage").addClass("error-msg");
                        $("#newslettererrormessage").show().html(response.message);
                        $("#newslettererrormessage").hide();
                    }
                });
            }
        }
        //NIVI Code For News Letter SignUp
        var firstName: string = $("#txtFirstName").val();
        if (firstName.length < 1) {
            $("#newslettererrormessage1").html(ZnodeBase.prototype.getResourceByKeyName("EnterFirstName"));
            $("#newslettererrormessage1").addClass("error-msg");
            $("#newslettererrormessage1").show();
            return false;
        }
        else {
            $("#newslettererrormessage1").html('');
            $("#newslettererrormessage1").removeClass("error-msg");
            $("#newslettererrormessage1").hide();
            $("#newslettererrormessage").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"))
            $("#newslettererrormessage").html(ZnodeBase.prototype.getResourceByKeyName("RequiredEmailId"))
            $("#newslettererrormessage").addClass("error-msg");
            $("#newslettererrormessage").show();
            return false;
        }
    }
    /*Nivi code end*/

    //Get Cart Count for Donut Caching
    public GetCartCount(): any {
        Endpoint.prototype.GetCartCount(function (response) {
            return $(".cartcount").val(response);
        });
    }
}
