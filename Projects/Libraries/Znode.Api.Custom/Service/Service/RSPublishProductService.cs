﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Api.Custom.Service.Service
{
    public class RSPublishProductService : PublishProductService, IRSPublishProductService
    {
        private readonly IMongoRepository<ConfigurableProductEntity> _configurableproductRepository;
        private readonly IMongoRepository<ProductEntity> _ProductMongoRepository;
        private readonly IZnodeRepository<ZnodePublishProduct> _publishProductRepository;
        private readonly IPublishProductHelper publishProductHelper;
        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        private readonly ISEOService _seoService;


        #region Constructor
        public RSPublishProductService() : base()
        {
            _ProductMongoRepository = new MongoRepository<ProductEntity>();
            _configurableproductRepository = new MongoRepository<ConfigurableProductEntity>();
            _publishProductRepository = new ZnodeRepository<ZnodePublishProduct>();
            publishProductHelper = ZnodeDependencyResolver.GetService<IPublishProductHelper>();
            _categoryMongoRepository = new MongoRepository<CategoryEntity>(GetCatalogVersionId());
            _seoService = new SEOService();

        }
        #endregion

        /// <summary>
        /// Store Locations fetching functionality for Store Locator,PDP and Cart
        /// </summary>
        /// <param name="state"></param>
        /// <param name="sku"></param>
        /// <returns></returns>
        public DataTable GetStoreLocations(string state, string sku)
        {
            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            objStoredProc.GetParameter("@State", state, ParameterDirection.Input, SqlDbType.VarChar);
            objStoredProc.GetParameter("@SKU", sku, ParameterDirection.Input, SqlDbType.VarChar);

            DataSet data = objStoredProc.GetSPResultInDataSet("RS_GetStoreAddressDetails");

            DataTable storeLocationTable = data.Tables[0];

            return storeLocationTable;
        }

        /// <summary>
        /// Store Locations fetching functionality for Store Locator,PDP and Cart
        /// </summary>
        /// <param name="state"></param>
        /// <param name="sku"></param>
        /// <returns></returns>
        public DataTable GetStoreLocationDetails(string state, string sku)
        {
            DataTable storeData = this.GetStoreLocations(state, sku);
            return storeData;
        }

        private PublishProductModel AddPersonalizeAttributeInChildProduct(PublishProductModel publishProduct, List<PublishAttributeModel> parentPersonalizableAttributes, bool isChildPersonalizableAttribute)
        {
            if (!isChildPersonalizableAttribute)
            {
                if (parentPersonalizableAttributes?.Count > 0)
                    publishProduct.Attributes.AddRange(parentPersonalizableAttributes);
            }

            return publishProduct;
        }
       
        public override PublishProductListModel GetPublishProductList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page, ParameterKeyModel parameters)
        {
          //  ZnodeLogging.LogMessage("Execution started.", string.Empty, TraceLevel.Info);
            PublishProductListModel publishProductListModel = new PublishProductListModel();

            switch (parameters.ParameterKey)
            {
                case ZnodeConstant.MongoId:
                    int catalogId, portalId, localeId;
                    GetParametersValueForFilters(filters, out catalogId, out portalId, out localeId);

                    ObjectId[] objectIDs = parameters.Ids.Split(',').Select(objectId => ObjectId.Parse(objectId)).ToArray();
                   
                    //Mongo product query by Object Id.
                    IMongoRepository<ProductEntity> _productMongoRepository = new MongoRepository<ProductEntity>(GetCatalogVersionId());
                    publishProductListModel.PublishProducts = new List<PublishProductModel>();
                    publishProductListModel.PublishProducts = _productMongoRepository.Table.MongoCollection.Find(Query<ProductEntity>.In(pr => pr.Id, objectIDs))?.ToModel<PublishProductModel>()?.ToList();

                    GetExpands(portalId, localeId, expands, publishProductListModel);
                    //get expands associated to Product
                    publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProductListModel, localeId, GetLoginUserId(), GetProfileId());
                    if (portalId > 0)
                    {                       
                        SetProductDetailsForList(portalId, publishProductListModel);
                    }
                    break;
                case ZnodeConstant.ZnodeCategoryIds:
                    filters.Add(new FilterTuple(FilterKeys.ZnodeCategoryIds, FilterOperators.In, parameters.Ids));
                    publishProductListModel = GetPublishProductList(expands, filters, sorts, page);
                    break;
                case ZnodeConstant.ProductSKU:
                    filters.Add(new FilterTuple(FilterKeys.SKU, FilterOperators.In, parameters.Ids));
                    publishProductListModel = GetPublishProductList(expands, filters, sorts, page);
                    break;
                default:
                    filters.Add(new FilterTuple(FilterKeys.ZnodeProductId, FilterOperators.In, parameters.Ids));
                    publishProductListModel = GetPublishProductList(expands, filters, sorts, page);
                    break;
            }
           // ZnodeLogging.LogMessage("Executed.", string.Empty, TraceLevel.Info);
            return publishProductListModel;
        }

        /// <summary>
        /// Overridden for complete PDP Swatch Image functionality is developed from here
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="portalId"></param>
        /// <param name="localeId"></param>
        /// <param name="publishProduct"></param>
        /// <param name="associatedProducts"></param>
        /// <param name="ConfigurableAttributeCodes"></param>
        /// <param name="catalogVersionId"></param>
        /// <returns></returns>
        protected override PublishProductModel GetDefaultConfiurableProduct(NameValueCollection expands, int portalId, int localeId, PublishProductModel publishProduct, List<ProductEntity> associatedProducts, List<string> ConfigurableAttributeCodes = null, int? catalogVersionId = 0)
        {
            //ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE-GETDEFAULTCONFIURABLEPRODUCT STARTED." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);

            string sku = publishProduct.SKU;
            string parentSEOCode = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == "SKU")?.AttributeValues;
            int categoryIds = publishProduct.ZnodeCategoryIds;
            List<PublishCategoryModel> categoryHierarchyIds = publishProduct.CategoryHierarchy;

            List<PublishAttributeModel> parentPersonalizableAttributes = publishProduct.Attributes?.Where(x => x.IsPersonalizable).ToList();
            string parentConfigurableProductName = publishProduct.Name;
            int configurableProductId = publishProduct.PublishProductId;

           

            //////Get first product from list of associated products
            //publishProduct = newassociatedProducts.FirstOrDefault().ToModel<PublishProductModel>();
            associatedProducts.ForEach(x => x.Attributes.Where(y => y.IsConfigurable).ToList()
           .ForEach(z =>
           {
               z.AttributeValues = z.SelectValues.FirstOrDefault()?.Value;
           }));
            publishProduct = associatedProducts.FirstOrDefault().ToModel<PublishProductModel>();
            publishProduct.ConfigurableProductId = configurableProductId;
            publishProduct.ParentSEOCode = parentSEOCode;
            publishProduct.ConfigurableProductSKU = publishProduct.SKU;
            publishProduct.CategoryHierarchy = categoryHierarchyIds;

            publishProduct.SKU = sku;
            publishProduct.ZnodeCategoryIds = categoryIds;
            publishProduct.IsConfigurableProduct = true;
            publishProduct.ParentConfiguarableProductName = parentConfigurableProductName;

            catalogVersionId = Convert.ToInt32(catalogVersionId) > 0 ? catalogVersionId : GetCatalogVersionId();

            publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId, WebstoreVersionId, GetProfileId());

            PublishAttributeModel defaultAttribute = publishProduct.Attributes?.FirstOrDefault(x => x.IsConfigurable);
            bool isChildPersonalizableAttribute = Convert.ToBoolean(publishProduct.Attributes?.Contains(publishProduct.Attributes?.FirstOrDefault(x => x.IsPersonalizable)));

            List<PublishAttributeModel> variants = publishProduct?.Attributes?.Where(x => x.IsConfigurable).ToList();
            Dictionary<string, string> selectedAttribute = GetSelectedAttributes(variants);

            //  List<PublishAttributeModel> attributeList = MapWebStoreConfigurableAttributeData(publishProductHelper.GetConfigurableAttributes(newassociatedProducts, ConfigurableAttributeCodes), defaultAttribute?.AttributeCode, defaultAttribute?.AttributeValues, selectedAttribute, newassociatedProducts, ConfigurableAttributeCodes, portalId);
            List<PublishAttributeModel> attributeList = MapWebStoreConfigurableAttributeData(publishProductHelper.GetConfigurableAttributes(associatedProducts, ConfigurableAttributeCodes), defaultAttribute?.AttributeCode, defaultAttribute?.AttributeValues, selectedAttribute, associatedProducts, ConfigurableAttributeCodes, portalId);

            foreach (PublishAttributeModel item in attributeList)
            {
                publishProduct.Attributes.RemoveAll(x => x.AttributeCode == item.AttributeCode);
            }

            publishProduct.Attributes.AddRange(attributeList);
            /*NIVI CODE*/
            string _swatchImagePath = ConfigurationManager.AppSettings["BaseImagePath"].ToString() + "t_97/";
            // ZnodeLogging.LogMessage("RSPublishProductService -_swatch Image Path line PDP379" + _swatchImagePath, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);

            List<AssociatedProductsModel> _products = (from p in associatedProducts

                                                       select new AssociatedProductsModel
                                                       {
                                                           PublishProductId = p.ZnodeProductId,
                                                           //PimProductId = pimProducts.FirstOrDefault(w => w.PublishProductId == p.ZnodeProductId)?.PimProductId,
                                                           SKU = p.SKU,
                                                           OMSColorCode = p.Attributes.Where(x => x.AttributeCode == "BaseGalleryImage1")?.FirstOrDefault()?.AttributeValues + "||" + p.Attributes.Where(x => x.AttributeCode == "BaseGalleryAltText1")?.FirstOrDefault()?.AttributeValues + "," +
                                                                          p.Attributes.Where(x => x.AttributeCode == "BaseGalleryImage2")?.FirstOrDefault()?.AttributeValues + "||" + p.Attributes.Where(x => x.AttributeCode == "BaseGalleryAltText2")?.FirstOrDefault()?.AttributeValues + "," +
                                                                          p.Attributes.Where(x => x.AttributeCode == "BaseGalleryImage3")?.FirstOrDefault()?.AttributeValues + "||" + p.Attributes.Where(x => x.AttributeCode == "BaseGalleryAltText3")?.FirstOrDefault()?.AttributeValues + "," +
                                                                          p.Attributes.Where(x => x.AttributeCode == "BaseGalleryImage4")?.FirstOrDefault()?.AttributeValues + "||" + p.Attributes.Where(x => x.AttributeCode == "BaseGalleryAltText4")?.FirstOrDefault()?.AttributeValues + ",",

                                                           //OMSColorCode = p.Attributes.Where(x => x.AttributeCode == ZnodeConstant.GalleryImages)?.FirstOrDefault()?.AttributeValues,
                                                           OMSColorValue = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Color")?.SelectValues[0].Code,
                                                           // OMSColorPath = _swatchImagePath + p.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues,
                                                           OMSColorPath = _swatchImagePath + p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValues,
                                                           DisplayOrder = p.DisplayOrder,
                                                           OMSColorSwatchText = p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValues,
                                                           //OMSColorSwatchText = p.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues,
                                                           Custom1 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Size")?.SelectValues[0].Code,
                                                           Custom2 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "ShoeWidth")?.SelectValues[0].Code,
                                                           Custom3 = "|" + p.Attributes.FirstOrDefault(y => y.AttributeCode == "Color")?.SelectValues[0].Code + "-" +
                                                                    p.Attributes.FirstOrDefault(y => y.AttributeCode == "Size")?.SelectValues[0].Code + "-" +
                                                                    p.Attributes.FirstOrDefault(y => y.AttributeCode == "ShoeWidth")?.SelectValues[0].Code
                                                       }).ToList();
            // ZnodeLogging.LogMessage("RSPublishProductService -_swatch Image Path line PDP415" + _products, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            publishProduct.AssociatedProducts = new List<AssociatedProductsModel>();
            publishProduct.AssociatedProducts.AddRange(_products);
            /*NIVI CODE*/
            return AddPersonalizeAttributeInChildProduct(publishProduct, parentPersonalizableAttributes, isChildPersonalizableAttribute);

        }
        /// <summary>
        /// Overridden to copy parent brand to child for schema.org
        /// </summary>
        /// <param name="publishProductId"></param>
        /// <param name="filters"></param>
        /// <param name="expands"></param>
        /// <returns></returns>
        public new PublishProductModel GetPublishProduct(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {
            try
            {
                GetParametersValueForFilters(filters, out int catalogId, out int portalId, out int localeId);

                ////Remove portal id filter.
                filters.RemoveAll(x => x.FilterName == FilterKeys.PortalId);

                ////Replace filter keys.
                ReplaceFilterKeys(ref filters);

                ////get catalog current version id by catalog id.
                int? catalogVersionId = GetCatalogVersionId(catalogId);

                //GetFilterForSubCategory(publishProductId, filters, expands);
                ///ReplaceFilterKeys(ref filters);
                filters.Add(WebStoreEnum.ZnodeProductId.ToString(), FilterOperators.Equals, Convert.ToString(publishProductId));

                //Get publish product from mongo
                List<ProductEntity> products = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()));
                //string brand = products[0].Attributes.FirstOrDefault(y => y.AttributeCode == "Brand")?.SelectValues[0].Value;

                filters.RemoveAll(x => x.FilterName == FilterKeys.ZnodeProductId);
                ReplaceFilterKeys(ref filters);
                filters.Add("portalid".ToString(), FilterOperators.Equals, Convert.ToString(PortalId));

                PublishProductModel publishProduct = base.GetPublishProduct(publishProductId, filters, expands);
                publishProduct.Attributes.RemoveAll(x => x.AttributeCode == "Brand");
                PublishAttributeModel publishAttributeModel = products[0].Attributes.FirstOrDefault(y => y.AttributeCode == "Brand").ToModel<PublishAttributeModel>();

                if (publishAttributeModel != null)
                {
                    publishProduct.Attributes.Add(publishAttributeModel);
                }
                return publishProduct;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("ConfigurableAttributes" + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
                throw ex;
            }
        }
        
      }
}
