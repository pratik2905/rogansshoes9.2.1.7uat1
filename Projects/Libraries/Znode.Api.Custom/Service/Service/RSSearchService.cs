﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;

namespace Znode.Api.Custom.Service.Service
{
    public class RSSearchService : SearchService
    {
        public RSSearchService()
        {

        }
        public override KeywordSearchModel FullTextSearch(SearchRequestModel model, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            /*NIVI CODE*/
            if (model.PageSize == 16)
                model.PageSize = 40;
            KeywordSearchModel searchResult = base.FullTextSearch(model, expands, filters, sorts, page);
            return searchResult;
        }
    }
}
