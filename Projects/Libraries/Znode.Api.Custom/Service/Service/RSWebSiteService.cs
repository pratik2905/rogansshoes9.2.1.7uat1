﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using Znode.Api.Custom.Helper;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Engine.Services.Helper;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using Znode.Libraries.MongoDB.Data.DataModel;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
namespace Znode.Api.Custom.Service.Service
{
    public class RSWebSiteService: WebSiteService
    {
        #region Private Variable
        private readonly IZnodeRepository<ZnodeCMSPortalTheme> _cmsPortalThemeRepository;
        private readonly IZnodeRepository<ZnodeCMSTheme> _cmsThemeRepository;
        private readonly IZnodeRepository<ZnodeCMSContentPage> _contentPageRepository;
        private readonly IZnodeRepository<ZnodeCMSContentPagesProfile> _cmsContentPagesProfileRepository;
        private readonly IZnodeRepository<ZnodeCMSPortalProductPage> _cmsPortalProductPageRepository;
        private readonly IZnodeRepository<ZnodeMedia> _mediaManagerRepository;
        private readonly IZnodeRepository<ZnodeCMSSlider> _cmsSliderRepository;
        private readonly IZnodeRepository<ZnodeCMSContentPagesLocale> _contentPagesLocaleRepository;
        private readonly IZnodeRepository<ZnodeCMSWidget> _widgetRepository;
        private readonly IZnodeRepository<ZnodeCMSWidgetCategory> _cmsWidgetCategoryRepository;
        private readonly IZnodeRepository<ZnodeCMSWidgetBrand> _cmsWidgetBrandRepository;
        private readonly IZnodeRepository<ZnodeCMSWidgetProduct> _cmsWidgetProductRepository;
        private readonly IZnodeRepository<ZnodePublishPortalLog> _publishPortalLogRepository;
        private readonly IZnodeRepository<ZnodePimCatalog> _catalogRepository;
        private readonly IZnodeRepository<ZnodePublishCatalog> _publishCatalogRepository;
        private readonly IZnodeRepository<ZnodePortalCatalog> _portalCatalogRepository;
        private readonly IZnodeRepository<ZnodeCMSThemeCSS> _cmsThemeCSSRepository;
        private readonly IZnodeRepository<ZnodeCMSTemplate> _cmsTemplateRepository;
        private readonly IZnodeRepository<ZnodeCMSMessage> _cmsMessageRepository;

        private readonly IMongoRepository<ProductPageEntity> _cmsProductPageMongoRepository;
        private readonly IMongoRepository<WebStoreEntity> _cmsWebStoreMongoRepository;
        private readonly IMongoRepository<WidgetCategoryEntity> _cmsWidgetCategoryMongoRepository;
        private readonly IMongoRepository<WidgetProductEntity> _cmsWidgetProductMongoRepository;
        private readonly IMongoRepository<WidgetSliderBannerEntity> _cmsWidgetSliderBannerMongoRepository;
        private readonly IMongoRepository<WidgetTitleEntity> _cmsWidgetTitleMongoRepository;
        private readonly IMongoRepository<TextWidgetEntity> _cmsTextWidgetMongoRepository;
        private readonly IMongoRepository<ContentPageConfigEntity> _cmsContentPageMongoRepository;
        private readonly IMongoRepository<MessageEntity> _cmsMessageMongoRepository;
        private readonly IMongoRepository<WidgetBrandEntity> _cmsWidgetBrandMongoRepository;
        private readonly IMongoRepository<BlogNewsEntity> _blogNewsMongoRepository;
        private readonly IMongoRepository<PortalGlobalAttributeEntity> _portalAttributMongoRepository;
        private readonly IMongoRepository<SeoEntity> _seoMongoRepository;
        private readonly IMongoRepository<_LogSeoEntity> log_seoMongoRepository;
        private readonly IMongoRepository<SearchWidgetEntity> _cmsSearchWidgetMongoRepository;
        #endregion

        public RSWebSiteService()
        {
            _cmsPortalThemeRepository = new ZnodeRepository<ZnodeCMSPortalTheme>();
            _contentPageRepository = new ZnodeRepository<ZnodeCMSContentPage>();
            _cmsContentPagesProfileRepository = new ZnodeRepository<ZnodeCMSContentPagesProfile>();
            _cmsThemeRepository = new ZnodeRepository<ZnodeCMSTheme>();
            _cmsPortalProductPageRepository = new ZnodeRepository<ZnodeCMSPortalProductPage>();
            _mediaManagerRepository = new ZnodeRepository<ZnodeMedia>();
            _cmsSliderRepository = new ZnodeRepository<ZnodeCMSSlider>();
            _widgetRepository = new ZnodeRepository<ZnodeCMSWidget>();
            _cmsWidgetCategoryRepository = new ZnodeRepository<ZnodeCMSWidgetCategory>();
            _cmsWidgetBrandRepository = new ZnodeRepository<ZnodeCMSWidgetBrand>();
            _cmsWidgetProductRepository = new ZnodeRepository<ZnodeCMSWidgetProduct>();
            _publishPortalLogRepository = new ZnodeRepository<ZnodePublishPortalLog>();
            _catalogRepository = new ZnodeRepository<ZnodePimCatalog>();
            _publishCatalogRepository = new ZnodeRepository<ZnodePublishCatalog>();
            _portalCatalogRepository = new ZnodeRepository<ZnodePortalCatalog>();
            _cmsThemeCSSRepository = new ZnodeRepository<ZnodeCMSThemeCSS>();
            _cmsTemplateRepository = new ZnodeRepository<ZnodeCMSTemplate>();
            _contentPagesLocaleRepository = new ZnodeRepository<ZnodeCMSContentPagesLocale>();
            _cmsMessageRepository = new ZnodeRepository<ZnodeCMSMessage>();

            _cmsProductPageMongoRepository = new MongoRepository<ProductPageEntity>();
            _cmsWebStoreMongoRepository = new MongoRepository<WebStoreEntity>();
            _cmsWidgetCategoryMongoRepository = new MongoRepository<WidgetCategoryEntity>();
            _cmsWidgetProductMongoRepository = new MongoRepository<WidgetProductEntity>();
            _cmsWidgetSliderBannerMongoRepository = new MongoRepository<WidgetSliderBannerEntity>();
            _cmsWidgetTitleMongoRepository = new MongoRepository<WidgetTitleEntity>();
            _cmsTextWidgetMongoRepository = new MongoRepository<TextWidgetEntity>();
            _cmsContentPageMongoRepository = new MongoRepository<ContentPageConfigEntity>();
            _cmsMessageMongoRepository = new MongoRepository<MessageEntity>();
            _cmsWidgetBrandMongoRepository = new MongoRepository<WidgetBrandEntity>();
            _blogNewsMongoRepository = new MongoRepository<BlogNewsEntity>();
            _portalAttributMongoRepository = new MongoRepository<PortalGlobalAttributeEntity>();
            _seoMongoRepository = new MongoRepository<SeoEntity>();
            log_seoMongoRepository = new MongoRepository<_LogSeoEntity>();
            _cmsSearchWidgetMongoRepository = new MongoRepository<SearchWidgetEntity>();
        }
        public override bool PublishAsync(int portalId, string targetPublishState = null, string publishContent = null, bool takeFromDraftFirst = false)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);

            HttpContext httpContext = HttpContext.Current;
            Thread thread = new Thread(new ThreadStart(() =>
            {
                HttpContext.Current = httpContext;

                PublishProcessor processor = new PublishProcessor();

                processor.PublishCompleteWebstore(portalId, CopyWebstoreWithinMongo, CopyWebstoreFromSQLToMongo, targetPublishState, publishContent, takeFromDraftFirst, true);

                ClearCacheHelper.ClearCacheAfterPublish(Convert.ToString(portalId));
                RSCloudflareHelper rSCloudflareHelper = new RSCloudflareHelper();
                rSCloudflareHelper.PurgeEverything();
            }));
            thread.Start();

            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

            return true;
        }

        #region Publish Sub methods
        private void RollbackPublishedCMSEntities(Exception ex, int versionToRollback)
        {
            ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.CMS.ToString(), TraceLevel.Error);
            PreviewHelper.DeleteAllCMSEntitiesFromMongo(versionToRollback);
        }

        //Update Publish Portal Status
        private void UpdatePortalPublishState(ZnodePublishPortalLog znodePublishPortalLog, ZnodePublishStatesEnum targetContentState)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            znodePublishPortalLog.PublishStateId = ((byte)targetContentState);
            _publishPortalLogRepository.Update(znodePublishPortalLog);
        }
        private void UpdatePublishStateForCompleteCMS(int portalId, int localeId, List<byte> sourceContentStates, ZnodePublishStatesEnum targetContentState)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, localeId.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, localeId });

            bool? status = null;

            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            executeSpHelper.GetParameter("@PortalId", portalId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@LocaleId", localeId, ParameterDirection.Input, SqlDbType.Int);
            executeSpHelper.GetParameter("@CurrentPublishStateId", string.Join(",", sourceContentStates), ParameterDirection.Input, SqlDbType.VarChar);
            executeSpHelper.GetParameter("@PublishStateId", (byte)targetContentState, ParameterDirection.Input, SqlDbType.TinyInt);
            executeSpHelper.GetParameter("@Status", status, ParameterDirection.Output, SqlDbType.Bit);
            executeSpHelper.GetSPResultInObject("Znode_UpdateStorePublishState");
            ZnodeLogging.LogMessage("Execution done.", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);

        }
        //Makes a copy of existing Store and CMS content to the given target publish state.
        private bool CopyWebstoreWithinMongo(WebStoreEntity existingWebstoreCopy, ZnodePublishStatesEnum targetPublishState, List<string> publishContent)
        {
            bool isSuccessful = false;
            Exception exception = new Exception();

            int sourceVersionId = existingWebstoreCopy.VersionId;

            int portalId = existingWebstoreCopy.PortalId;
            int localeId = existingWebstoreCopy.LocaleId;
            ZnodePublishStatesEnum sourceContentState;
            Enum.TryParse(existingWebstoreCopy.PublishState, true, out sourceContentState);
            ZnodeLogging.LogMessage("sourceVersionId, portalId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { sourceVersionId, portalId, localeId });

            ZnodePublishPortalLog znodePublishPortalLog = null;

            ZnodePublishPortalLog previewedPortalLog = _publishPortalLogRepository.GetById(sourceVersionId);
            if (HelperUtility.IsNotNull(previewedPortalLog))
                znodePublishPortalLog = _publishPortalLogRepository.Insert(
                    new ZnodePublishPortalLog { PublishStateId = previewedPortalLog.PublishStateId, PortalId = portalId, IsPortalPublished = previewedPortalLog.IsPortalPublished });

            int currentPublishedVersionId = 0;
            if (IsNotNull(znodePublishPortalLog))
                currentPublishedVersionId = znodePublishPortalLog.PublishPortalLogId;

            WebStoreEntity previousPublishedWebstore = PreviewHelper.GetWebstore(portalId, targetPublishState, localeId);

            try
            {
                PreviewHelper.CopyAllCMSEntitiesToVersion(sourceVersionId, currentPublishedVersionId, portalId, targetPublishState);

                isSuccessful = true;
            }
            catch (ZnodeException ex)
            {
                isSuccessful = false;
                RollbackPublishedCMSEntities(ex, currentPublishedVersionId);
                exception = ex;
            }
            catch (MongoException ex)
            {
                isSuccessful = false;
                RollbackPublishedCMSEntities(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.MongoExceptionDuringPublish, Api_Resources.MongoExceptionMessageDuringPublish);
            }
            catch (System.Data.Entity.Core.EntityException ex)
            {
                isSuccessful = false;
                RollbackPublishedCMSEntities(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.EntityExceptionDuringPublish, Api_Resources.EntityExceptionMessageDuringPublish);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                isSuccessful = false;
                RollbackPublishedCMSEntities(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.SQLExceptionDuringPublish, Api_Resources.SQLExceptionMessageDuringPublish);
            }
            catch (Exception ex)
            {
                isSuccessful = false;
                RollbackPublishedCMSEntities(ex, currentPublishedVersionId);
                exception = new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, Api_Resources.GenericExceptionMessageDuringPublish);
            }

            if (!isSuccessful)
                throw exception;
            else
            {
                //Update CMS content publish status only if included in publish.
                if (IsNull(publishContent) || publishContent.Count == 0 || publishContent.Contains(ZnodePublishContentTypeEnum.CmsContent.ToString()))
                    UpdatePublishStateForCompleteCMS(portalId, localeId, Enum.GetValues(typeof(ZnodePublishStatesEnum)).Cast<ZnodePublishStatesEnum>().Select(x => (byte)x).ToList(), targetPublishState);

                //Update Store publish status only if included in publish.
                if (IsNull(publishContent) || publishContent.Count == 0 || publishContent.Contains(ZnodePublishContentTypeEnum.StoreSettings.ToString()))
                    UpdatePortalPublishState(znodePublishPortalLog, targetPublishState);

                //Delete previous published copy from mongo at last.
                if (IsNotNull(previousPublishedWebstore))
                    PreviewHelper.DeleteAllCMSEntitiesFromMongo(previousPublishedWebstore.VersionId, null, true);

                PreviewHelper.CreatePreviewLog(targetPublishState.ToString(), currentPublishedVersionId, IsNotNull(previousPublishedWebstore) ? previousPublishedWebstore.VersionId : 0, portalId, "Webstore", localeId, GetLocaleName(localeId));

                return true;
            }
        }
        //Save CMS Portal Product Page to mongo as preview.
        private void SavePortalProductPagesToMongo(int portalId, int versionId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, versionId:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, versionId });

            List<ProductPageEntity> portalProductPages = _cmsPortalProductPageRepository.Table.Where(x => x.PortalId == portalId)?.ToModel<ProductPageEntity>()?.ToList();
            ZnodeLogging.LogMessage("portalProductPages count :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalProductPages?.Count });

            if (portalProductPages?.Count > 0)
                _cmsProductPageMongoRepository.Create(portalProductPages.Select(c => { c.VersionId = versionId; return c; }).ToList());
        }
        //Publish CMS Widget Category to mongo
        private void SaveWidgetCategoryToMongo(int portalId, List<int> contentPageIds, int versionId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, contentPageIds, versionId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, contentPageIds, versionId });

            if (_cmsWidgetCategoryRepository.Table.Any(
                                          x => (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.PortalMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && x.CMSMappingId == portalId)
                                          || (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && contentPageIds.Contains(x.CMSMappingId))))
            {
                List<WidgetCategoryEntity> widgetCategoryConfigurations = _cmsWidgetCategoryRepository.Table.Where(
                                          x => (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.PortalMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && x.CMSMappingId == portalId)
                                          || (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && contentPageIds.Contains(x.CMSMappingId)))?.ToModel<WidgetCategoryEntity>().ToList();

                if (widgetCategoryConfigurations?.Count > 0)
                    _cmsWidgetCategoryMongoRepository.Create(widgetCategoryConfigurations.Select(w => { w.PortalId = portalId; w.VersionId = versionId; return w; }).ToList());
            }
        }
        //Publish CMS Widget Product to mongo
        private void SaveWidgetProductToMongo(int portalId, List<int> contentPageIds, int versionId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, contentPageIds, versionId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, contentPageIds, versionId });

            List<WidgetProductEntity> widgetProductConfigurations = _cmsWidgetProductRepository.Table.Where(
                                          x => (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.PortalMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && x.CMSMappingId == portalId)
                                          || (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && contentPageIds.Contains(x.CMSMappingId)))?.ToModel<WidgetProductEntity>()?.ToList();

            if (widgetProductConfigurations?.Count > 0)
                _cmsWidgetProductMongoRepository.Create(widgetProductConfigurations.Select(w => { w.PortalId = portalId; w.VersionId = versionId; return w; }).ToList());
        }
        //Publish CMS Widget Title to mongo
        private void SaveTitleWidgetToMongo(int portalId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, versionId, localeId });

            IZnodeViewRepository<WidgetTitleEntity> objStoredProc = new ZnodeViewRepository<WidgetTitleEntity>();

            //SP parameters for getting date to be publish.
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@LocaleId", localeId, ParameterDirection.Input, DbType.Int32);

            List<WidgetTitleEntity> widgetTitleConfigurations;
            widgetTitleConfigurations = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishCMSWidgetTitle  @PortalId, @LocaleId").ToList();

            widgetTitleConfigurations?.ForEach(x => x.VersionId = versionId);
            if (widgetTitleConfigurations?.Count > 0)
                _cmsWidgetTitleMongoRepository.Create(widgetTitleConfigurations);
        }
        //Publish CMS Text Widget Configuration to mongo
        private void SaveTextWidgetConfigurationToMongo(int portalId, int userId, int versionId, int localeId)
            => PublishHelper.SaveToMongo<TextWidgetEntity>(PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetTextWidgetConfiguration", null, "", 0, "@LocaleId", localeId), _cmsTextWidgetMongoRepository, versionId);

        //Publish CMS Search widget data.
        private void SaveSearchWidgetConfigurationToMongo(int portalId, int userId, int versionId, int localeId)
            => PublishHelper.SaveToMongo<SearchWidgetEntity>(PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetSearchWidgetConfiguration", null, "", 0, "@LocaleId", localeId), _cmsSearchWidgetMongoRepository, versionId);
        //Publish CMS Widget Slider Banner to mongo
        private void SaveWidgetSliderBannerToMongo(int portalId, int userId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, userId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, userId, versionId, localeId });

            DataSet dataSet = PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetCMSWidgetSliderBanner", null, "", 0, "@LocaleId", localeId);
            DataTable dataTable = dataSet.Tables[0];
            foreach (DataRow row in dataTable.Rows)
            {
                WidgetSliderBannerEntity entity = ConvertXMLStringToModel<WidgetSliderBannerEntity>(Convert.ToString(row["ReturnXML"]));
            }
            PublishHelper.SaveToMongo<WidgetSliderBannerEntity>(dataSet, _cmsWidgetSliderBannerMongoRepository, versionId);
        }
        //Publish CMS Message Configuration to mongo
        private void SaveMessagesToMongo(int portalId, int userId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, userId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, userId, versionId, localeId });

            DataSet dataSet = PublishHelper.ExecuteSP(portalId, "@PortalId", userId, "Znode_GetCMSMessageConfiguration", null, "", 0, "@LocaleId", localeId);
            DataTable dataTable = dataSet.Tables[0];
            foreach (DataRow row in dataTable.Rows)
            {
                var doc = new XmlDocument();

                doc.LoadXml(Convert.ToString(row["ReturnXML"]));
            }
            PublishHelper.SaveToMongo<MessageEntity>(dataSet, _cmsMessageMongoRepository, versionId);
        }

        private List<ContentPageConfigEntity> GetContentPagesForLocale(int portalId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Input parameters portalId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, versionId, localeId });

            int defaultLocaleId = GetDefaultLocaleId();
            return (from contentpage in _contentPageRepository.Table
                    join template in _cmsTemplateRepository.Table on contentpage.CMSTemplateId equals template.CMSTemplateId
                    join contentPagesLocale in _contentPagesLocaleRepository.Table on contentpage.CMSContentPagesId equals contentPagesLocale.CMSContentPagesId
                    where contentpage.PortalId == portalId && (contentPagesLocale.LocaleId == localeId || contentPagesLocale.LocaleId == defaultLocaleId)
                    select new ContentPageConfigEntity
                    {
                        ContentPageId = contentpage.CMSContentPagesId,
                        PortalId = portalId,
                        FileName = template.FileName,
                        PageName = contentpage.PageName,
                        PageTitle = contentPagesLocale.PageTitle,
                        ActivationDate = contentpage.ActivationDate,
                        ExpirationDate = contentpage.ExpirationDate,
                        VersionId = versionId,
                        LocaleId = localeId,
                        IsActive = contentpage.IsActive
                    }).ToList();
        }

        //Publish CMS Content Page Configuration to mongo
        private void SaveContentPageConfigurationToMongo(int portalId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, versionId, localeId });

            //Get content pages associated to portalId
            List<ContentPageConfigEntity> ContentPageConfigconfigurations = GetContentPagesForLocale(portalId, versionId, localeId);
            List<int> contentPageIds = ContentPageConfigconfigurations.Select(x => x.ContentPageId).ToList();
            ZnodeLogging.LogMessage("contentPageIds associated to portal:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { contentPageIds });

            //Get porfiles associated to content pages
            List<ZnodeCMSContentPagesProfile> contentPagesProfiles =
                _cmsContentPagesProfileRepository.Table.Where(x => contentPageIds.Contains(x.CMSContentPagesId)).ToList();

            ZnodeLogging.LogMessage("profiles associated to content pages:", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { contentPagesProfiles });

            foreach (ContentPageConfigEntity ContentPageConfigconfiguration in ContentPageConfigconfigurations)
            {
                //Profileds Associated to content page
                int?[] profileIds = contentPagesProfiles.Where(x => x.CMSContentPagesId == ContentPageConfigconfiguration.ContentPageId && x.ProfileId != null).Select(x => x.ProfileId).ToArray();
                ContentPageConfigconfiguration.ProfileId = profileIds.Length > 0 ? profileIds : null;
            }

            if (ContentPageConfigconfigurations?.Count > 0)
                _cmsContentPageMongoRepository.Create(ContentPageConfigconfigurations);
        }


        //Publish blog/news to mongo.
        private void SaveBlogNewsToMongo(int portalId, int versionId, int localeId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, versionId, localeId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, versionId, localeId });

            IZnodeViewRepository<BlogNewsEntity> objStoredProc = new ZnodeViewRepository<BlogNewsEntity>();

            //SP parameters for getting data to be published.
            objStoredProc.SetParameter("@PortalId", portalId, ParameterDirection.Input, DbType.Int32);
            objStoredProc.SetParameter("@LocaleId", localeId, ParameterDirection.Input, DbType.Int32);

            List<BlogNewsEntity> blogNewsEntities;
            blogNewsEntities = objStoredProc.ExecuteStoredProcedureList("Znode_GetPublishBlogNews  @PortalId, @LocaleId").ToList();
            ZnodeLogging.LogMessage("blogNewsEntities count :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { blogNewsEntities?.Count });

            blogNewsEntities?.ForEach(x => x.VersionId = versionId);
            if (blogNewsEntities?.Count > 0)
                _blogNewsMongoRepository.Create(blogNewsEntities);
        }
        //Publish CMS Widget Brand to mongo
        private void SaveWidgetBrandToMongo(int portalId, List<int> contentPageIds, int versionId)
        {
            ZnodeLogging.LogMessage("Execution started :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Info);
            ZnodeLogging.LogMessage("Input parameters portalId, contentPageIds, versionId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { portalId, contentPageIds, versionId });

            List<WidgetBrandEntity> widgetBrandConfigurations = _cmsWidgetBrandRepository.Table.Where(
                                          x => (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.PortalMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && x.CMSMappingId == portalId)
                                          || (x.TypeOFMapping.Equals(ZnodeCMSTypeofMappingEnum.ContentPageMapping.ToString(), StringComparison.OrdinalIgnoreCase)
                                          && contentPageIds.Contains(x.CMSMappingId)))?.ToModel<WidgetBrandEntity>()?.ToList();
            if (widgetBrandConfigurations?.Count > 0)
                _cmsWidgetBrandMongoRepository.Create(widgetBrandConfigurations.Select(w => { w.PortalId = portalId; w.VersionId = versionId; return w; }).ToList());
        }
        // Pushes the Store and CMS content found in SQL database to the supplied target state in Mongo database.
        private bool CopyWebstoreFromSQLToMongo(int portalId, int localeId, ZnodePublishStatesEnum targetPublishState, List<string> publishContent)
        {
            bool isSuccessful = false;
            Exception exception = new Exception();

            ZnodePublishPortalLog znodePublishPortalLog;

            WebStoreEntity existingWebstoreCopy = PreviewHelper.GetWebstore(portalId, targetPublishState, localeId);

            int? previousWebstoreVersion = existingWebstoreCopy?.VersionId;
            ZnodeLogging.LogMessage("previousWebstoreVersion :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { previousWebstoreVersion });

            int newWebstoreVersion = 0;

            SEODetailsEnum[] seoTypes = new SEODetailsEnum[] { SEODetailsEnum.BlogNews, SEODetailsEnum.Content_Page };

            try
            {
                int userId = GetLoginUserId();
                ZnodeLogging.LogMessage("userId returned from GetLoginUserId :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { userId });

                //Insert the PublishPortalLog for webstore preview for the first time cases.
                znodePublishPortalLog = _publishPortalLogRepository.Insert(new ZnodePublishPortalLog { PublishStateId = (byte)ZnodePublishStatesEnum.DRAFT, PortalId = portalId });
                newWebstoreVersion = znodePublishPortalLog.PublishPortalLogId;

                // Publishing Store Settings.
                if (IsNull(publishContent) || (publishContent.Count == 0) || publishContent.Contains(ZnodePublishContentTypeEnum.StoreSettings.ToString()) || !previousWebstoreVersion.HasValue)
                {
                    //Insert CMS Website Configuration to mongo for preview.
                    PushWebsiteConfigurationData(portalId, newWebstoreVersion, localeId, targetPublishState);
                    UpdatePortalPublishState(znodePublishPortalLog, targetPublishState);
                }
                else
                    PreviewHelper.CopyWebstoreToState(_cmsWebStoreMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId, targetPublishState);


                //Publishing CMS and SEO content.
                if (IsNull(publishContent) || (publishContent.Count == 0) || publishContent.Contains(ZnodePublishContentTypeEnum.CmsContent.ToString()) || !previousWebstoreVersion.HasValue)
                {
                    //Active Content Page Ids Associated with Portal ID
                    List<int> contentPageIds = _contentPageRepository.Table.Where(x => x.PortalId == portalId && x.IsActive).Select(x => x.CMSContentPagesId)?.ToList() ?? new List<int>();
                    ZnodeLogging.LogMessage("Active Content Page Ids Associated with Portal ID :", ZnodeLogging.Components.CMS.ToString(), TraceLevel.Verbose, new object[] { contentPageIds });

                    //Publish CMS Portal Product Page to mongo
                    SavePortalProductPagesToMongo(portalId, newWebstoreVersion);

                    //Publish CMS Widget Category to mongo
                    SaveWidgetCategoryToMongo(portalId, contentPageIds, newWebstoreVersion);

                    //Publish CMS Widget Product to mongo
                    SaveWidgetProductToMongo(portalId, contentPageIds, newWebstoreVersion);

                    //Publish CMS Widget Title to mongo
                    SaveTitleWidgetToMongo(portalId, newWebstoreVersion, localeId);

                    //Publish CMS Widget Slider Banner to mongo
                    SaveWidgetSliderBannerToMongo(portalId, userId, newWebstoreVersion, localeId);

                    //Publish CMS Text Widget Configuration to mongo
                    SaveTextWidgetConfigurationToMongo(portalId, userId, newWebstoreVersion, localeId);

                    //Publish CMS Text Widget Configuration to mongo
                    SaveSearchWidgetConfigurationToMongo(portalId, userId, newWebstoreVersion, localeId);

                    //Publish CMS Messages to mongo
                    SaveMessagesToMongo(portalId, userId, newWebstoreVersion, localeId);

                    //Publish CMS Content Page Configuration to mongo
                    SaveContentPageConfigurationToMongo(portalId, newWebstoreVersion, localeId);

                    //Publish Blog/News to mongo.
                    SaveBlogNewsToMongo(portalId, newWebstoreVersion, localeId);

                    //Publish CMS Widget Brand to mongo
                    SaveWidgetBrandToMongo(portalId, contentPageIds, newWebstoreVersion);

                    //Publish Portal Associted Global Attribute Details to Mongo
                    SavePortalAssociateAttributesToMongo(portalId, userId, newWebstoreVersion, localeId);

                    //Publish Seo to mongo
                    SaveSeoToMongo(portalId, localeId, newWebstoreVersion, (previousWebstoreVersion.HasValue ? previousWebstoreVersion.Value : 0), targetPublishState, seoTypes, null, false);
                }
                else
                {
                    PreviewHelper.CopyEntityFromVersion(_cmsProductPageMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsWidgetCategoryMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsWidgetProductMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsWidgetSliderBannerMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsWidgetTitleMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsTextWidgetMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsContentPageMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsMessageMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_cmsWidgetBrandMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_blogNewsMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    PreviewHelper.CopyEntityFromVersion(_portalAttributMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                    foreach (int seoType in seoTypes)
                    {
                        PreviewHelper.CopyEntityFromVersion(_seoMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId, Query<SeoEntity>.EQ(x => x.CMSSEOTypeId, seoType));
                    }

                    PreviewHelper.CopyEntityFromVersion(_cmsSearchWidgetMongoRepository, previousWebstoreVersion.Value, newWebstoreVersion, portalId);
                }
                isSuccessful = true;
            }
            catch (ZnodeException ex)
            {
                isSuccessful = false;
                RollbackSeoDetailFromMongo(portalId, newWebstoreVersion, localeId, seoTypes);
                RollbackPublishedCMSEntities(ex, newWebstoreVersion);
                exception = ex;
            }
            catch (MongoException ex)
            {
                isSuccessful = false;
                RollbackSeoDetailFromMongo(portalId, newWebstoreVersion, localeId, seoTypes);
                RollbackPublishedCMSEntities(ex, newWebstoreVersion);
                exception = new ZnodeException(ErrorCodes.MongoExceptionDuringPublish, Api_Resources.MongoExceptionMessageDuringPublish);
            }
            catch (System.Data.Entity.Core.EntityException ex)
            {
                isSuccessful = false;
                RollbackSeoDetailFromMongo(portalId, newWebstoreVersion, localeId, seoTypes);
                RollbackPublishedCMSEntities(ex, newWebstoreVersion);
                exception = new ZnodeException(ErrorCodes.EntityExceptionDuringPublish, Api_Resources.EntityExceptionMessageDuringPublish);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                isSuccessful = false;
                RollbackSeoDetailFromMongo(portalId, newWebstoreVersion, localeId, seoTypes);
                RollbackPublishedCMSEntities(ex, newWebstoreVersion);
                exception = new ZnodeException(ErrorCodes.SQLExceptionDuringPublish, Api_Resources.SQLExceptionMessageDuringPublish);
            }
            catch (Exception ex)
            {
                isSuccessful = false;
                RollbackSeoDetailFromMongo(portalId, newWebstoreVersion, localeId, seoTypes);
                RollbackPublishedCMSEntities(ex, newWebstoreVersion);
                exception = new ZnodeException(ErrorCodes.GenericExceptionDuringPublish, Api_Resources.GenericExceptionMessageDuringPublish);
            }

            if (!isSuccessful)
                throw exception;
            else
            {
                if (IsNull(publishContent) || (publishContent.Count == 0) || publishContent.Contains(ZnodePublishContentTypeEnum.CmsContent.ToString()) || !previousWebstoreVersion.HasValue)
                {
                    CommitSeoDetailToMongo(portalId, (previousWebstoreVersion.HasValue ? previousWebstoreVersion.Value : 0), localeId, seoTypes);

                    UpdatePublishStateForCompleteCMS(portalId, localeId, Enum.GetValues(typeof(ZnodePublishStatesEnum)).Cast<ZnodePublishStatesEnum>().Select(x => (byte)x).ToList(), targetPublishState);
                }

                //Delete all the previously previewed Configurations.
                if (previousWebstoreVersion.HasValue)
                    PreviewHelper.DeleteAllCMSEntitiesFromMongo(previousWebstoreVersion.Value, null, true);

                PreviewHelper.CreatePreviewLog(targetPublishState.ToString(), newWebstoreVersion, (previousWebstoreVersion.HasValue ? previousWebstoreVersion.Value : 0), portalId, "Webstore", localeId, GetLocaleName(localeId));

                return true;
            }
        }

        //Publish CMS Website Configuration to mongo
        private void PushWebsiteConfigurationData(int portalId, int versionId, int localeId, ZnodePublishStatesEnum targetPublishState)
        {
            WebStoreEntity websiteConfiguration = (from cmsPortalTheme in _cmsPortalThemeRepository.Table
                                                   join cmsTheme in _cmsThemeRepository.Table on cmsPortalTheme.CMSThemeId equals cmsTheme.CMSThemeId
                                                   join cmsThemeCSS in _cmsThemeCSSRepository.Table on cmsPortalTheme.CMSThemeCSSId equals cmsThemeCSS.CMSThemeCSSId
                                                   join mediaManager in _mediaManagerRepository.Table on cmsPortalTheme.MediaId equals mediaManager.MediaId into media
                                                   join faviconMedia in _mediaManagerRepository.Table on cmsPortalTheme.FavIconId equals faviconMedia.MediaId into favicon
                                                   from mediaManager in media.DefaultIfEmpty()
                                                   from faviconMedia in favicon.DefaultIfEmpty()
                                                   where cmsPortalTheme.PortalId == portalId
                                                   select new WebStoreEntity
                                                   {
                                                       PortalThemeId = cmsPortalTheme.CMSPortalThemeId,
                                                       PortalId = cmsPortalTheme.PortalId,
                                                       WebsiteLogo = mediaManager.Path,
                                                       ThemeId = cmsPortalTheme.CMSThemeId,
                                                       ThemeName = cmsTheme.Name,
                                                       WebsiteTitle = cmsPortalTheme.WebsiteTitle,
                                                       CSSId = cmsThemeCSS.CMSThemeCSSId,
                                                       CSSName = cmsThemeCSS.CSSName,
                                                       FaviconImage = faviconMedia.Path,
                                                       VersionId = versionId,
                                                       PublishState = targetPublishState.ToString(),
                                                       LocaleId = localeId
                                                   }).FirstOrDefault();

            if (IsNotNull(websiteConfiguration))
                _cmsWebStoreMongoRepository.Create(websiteConfiguration);
        }
        #endregion
    }
}
