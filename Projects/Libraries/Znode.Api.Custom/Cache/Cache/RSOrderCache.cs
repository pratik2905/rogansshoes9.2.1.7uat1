﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Cache.ICache;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Sample.Api.Model.Request;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
namespace Znode.Api.Custom.Cache.Cache
{
    public class RSOrderCache : BaseCache, IRSOrderCache
    {
        #region Private Variables
        private readonly IOrderService _orderService;
        #endregion

        #region Constructor
        public RSOrderCache(IOrderService orderService)
        {
            _orderService = orderService;
        }
        #endregion  

        public virtual string GetOrderList(RSOrderRequestModel OrderListRequest, string routeUri, string routeTemplate)
        {
            string data = GetFromCache(routeUri);
            if (string.IsNullOrEmpty(data))
            {
                //Get data from service
                FilterCollection filters = new FilterCollection();
                if (OrderListRequest.OrderStatus != null)
                    filters.Add(new FilterTuple("OrderState", FilterOperators.Like, OrderListRequest.OrderStatus));
                else if (OrderListRequest.StartDate != null || OrderListRequest.EndDate != null)
                {
                    filters.Add(new FilterTuple("OrderDate", FilterOperators.GreaterThanOrEqual, Convert.ToString("'" + OrderListRequest.StartDate + "'")));
                    filters.Add(new FilterTuple("OrderDate", FilterOperators.LessThanOrEqual, Convert.ToString("'" + OrderListRequest.EndDate + "'")));
                }
                OrdersListModel orderList = _orderService.GetOrderList(Expands, filters, Sorts, Page);
                if (orderList?.Orders?.Count > 0 || IsNotNull(orderList?.CustomerName))
                {
                    OrderListResponse response = new OrderListResponse { OrderList = orderList };
                    response.MapPagingDataFromModel(orderList);
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
    }
}
