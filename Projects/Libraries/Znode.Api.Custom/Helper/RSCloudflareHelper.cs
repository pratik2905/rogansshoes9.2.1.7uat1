﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Helper
{
    public class RSCloudflareHelper
    {
        public bool EnableCloudflarePurging
        {
            get
            {
                try
                {
                    return Convert.ToBoolean(ConfigurationManager.AppSettings["EnableCloudflarePurging"].ToString());
                }
                catch
                {
                    return false;
                }
            }
        }

        public string CloudflarePurgeAPIEndpoint
        {
            get
            {
                return $"{ConfigurationManager.AppSettings["CloudflarePurgeAPIEndpoint"].ToString().Replace("{CloudflareZoneIdentifier}", CloudflareZoneIdentifier)}";
            }
        }

        public string CloudflareDomainUrl
        {
            get
            {
                return $"{ConfigurationManager.AppSettings["CloudflareDomainUrl"].ToString()}";
            }
        }

        public string CloudflareZoneIdentifier
        {
            get
            {
                return $"{ConfigurationManager.AppSettings["CloudflareZoneIdentifier"].ToString()}";
            }
        }

        public string AccessToken
        {
            get
            {
                return $"{ConfigurationManager.AppSettings["CloudflareAccessToken"].ToString()}";
            }
        }

        /// <summary>
        /// Purge Cloudflare cache only for the supplied array of URLs/Files.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool PurgeFiles(CloudflarePurgeRequestModel model = null)
        {
            try
            {
                if (EnableCloudflarePurging)
                {
                    if (HelperUtility.IsNotNull(model?.Files) && model?.Files.Count > 0)
                    {
                        model.Files = model.Files.Select(x => $"{CloudflareDomainUrl}/{x}").ToList();

                        string responseString = PostToCloudflare(JsonConvert.SerializeObject(model));
                        //Deserialize to model.
                        CloudflarePurgeResponseModel result = JsonConvert.DeserializeObject<CloudflarePurgeResponseModel>(responseString);

                        return result.Success;
                    }
                    else
                    {
                        ZnodeLogging.LogMessage("No files were passed for Cloudflare purge.", "CloudflareHelper_PurgeFiles", System.Diagnostics.TraceLevel.Error);
                        return false;
                    }
                }
                else
                {
                    ZnodeLogging.LogMessage("Cloudflare purging is disabled.", "CloudflareHelper_PurgeFiles", System.Diagnostics.TraceLevel.Info);
                    return false;
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, "CloudflareHelper_PurgeFiles", System.Diagnostics.TraceLevel.Error, model);

                return false;
            }
        }

        /// <summary>
        /// Purge entire Cloudflare cache. Use carefully, since it slows down the performance temporarily.
        /// </summary>
        /// <returns></returns>
        public bool PurgeEverything()
        {
            try
            {
                if (EnableCloudflarePurging)
                {
                    string responseString = PostToCloudflare(JsonConvert.SerializeObject(new { purge_everything = true }));

                    //Deserialize to model.
                    CloudflarePurgeResponseModel result = JsonConvert.DeserializeObject<CloudflarePurgeResponseModel>(responseString);
                    ZnodeLogging.LogMessage("cloudfare"+result.Success.ToString(), "CloudflareHelper_PurgeEverything", System.Diagnostics.TraceLevel.Error);
                    return result.Success;
                }
                else
                {
                    ZnodeLogging.LogMessage("Cloudflare purging is disabled.", "CloudflareHelper_PurgeEverything", System.Diagnostics.TraceLevel.Info);
                    return false;
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, "CloudflareHelper_PurgeEverything", System.Diagnostics.TraceLevel.Error);

                return false;
            }
        }

        private string PostToCloudflare(string dataString = null)
        {
            byte[] data = Encoding.UTF8.GetBytes(dataString);

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(this.CloudflarePurgeAPIEndpoint);
            req.KeepAlive = false; // Prevents "server committed a protocol violation" error
            req.Method = "POST";
            req.ContentType = "application/json";

            //Set header for api request
            req.Headers.Add("Authorization", $"Bearer {this.AccessToken}");

            req.ContentLength = data.Length;

            using (var stream = req.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            WebResponse response = (HttpWebResponse)req.GetResponse();

            string responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return responseString;
        }
    }

    public class CloudflarePurgeRequestModel
    {
        [JsonProperty("files")]
        public List<string> Files { get; set; }
    }

    public class CloudflarePurgeResponseModel
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("errors")]
        public CloudflareResponseErrorModel[] Errors { get; set; }
    }

    public class CloudflareResponseErrorModel
    {
        [JsonProperty("code")]
        public string Code { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}

