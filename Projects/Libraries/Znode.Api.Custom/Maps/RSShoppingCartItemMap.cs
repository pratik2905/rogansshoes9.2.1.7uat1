﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using System.Configuration;
namespace Znode.Api.Custom.Maps
{
    public class RSShoppingCartItemMap: ShoppingCartItemMap
    {
        string BasePath = ConfigurationManager.AppSettings["BaseImagePath"].ToString() + "t_97/";
        public override string GetImagePath(string imageName, int portalId, IImageHelper objImage = null)
        {          
            return BasePath + imageName;
        }
    }
}
