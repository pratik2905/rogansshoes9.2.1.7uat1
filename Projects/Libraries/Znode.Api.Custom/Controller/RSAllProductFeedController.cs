﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Znode.Api.Custom.Service.IService;
using Znode.Api.Custom.Service.Service;
using Znode.Engine.Api.Controllers;
using Znode.Engine.Api.Models.Responses;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Controller
{
    public class RSAllProductFeedController : BaseController
    {
        #region Private Variables

        private readonly IRSAllProductFeedService _service;

        #endregion

        public RSAllProductFeedController()
        {
            _service = new RSAllProductFeedService();
        }
        #region Public Method
        /// <summary>
        /// Generates an XML file 
        /// </summary>
        /// <param name="model">Product Feed Model</param>
        /// <returns> It will return an Xml file which content all the detail about the product,category and content</returns>
        [ResponseType(typeof(HttpStatusCode))]
        [HttpPost]
        public HttpResponseMessage Create()
        {
            HttpResponseMessage response;
            try
            {

                bool isSuccess = _service.CreateGoogleSiteMapforall();
                if (isSuccess)
                {
                    response = CreateCreatedResponse(Request.CreateResponse(HttpStatusCode.OK));
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.Admin.ToString(), TraceLevel.Error);
                response = CreateInternalServerErrorResponse(new ProductFeedResponse { HasError = true, ErrorMessage = ex.Message });
            }
            return response;
        }
        #endregion
    }
}
