﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Engine.WebStore;
using Znode.Libraries.Resources;
using Znode.Engine.Api.Client.Expands;
using Znode.WebStore.Custom.Helper;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSProductAgent : ProductAgent, IProductAgent
    {
        #region Private Variables
        private readonly IPublishProductClient _productClient;
        #endregion

        #region Public Constructor
        public RSProductAgent(ICustomerReviewClient reviewClient, IPublishProductClient productClient, IWebStoreProductClient webstoreProductClient, ISearchClient searchClient, IHighlightClient highlightClient, IPublishCategoryClient publishCategoryClient):
            base(reviewClient,productClient,webstoreProductClient,searchClient,highlightClient,publishCategoryClient)
        {            
            _productClient = GetClient<IPublishProductClient>(productClient);            
        }
        #endregion

        //public override ProductViewModel GetConfigurableProduct(ParameterProductModel model)
        //{
        //    try
        //    {              
        //        ProductViewModel viewModel = base.GetConfigurableProduct(model);            
        //        if (viewModel != null)
        //        {
        //            string[] codes = model.Codes.Split(',');
        //            int colorIndex = Array.IndexOf(codes, "Color");
        //            string selectedcolor = model.Values.Split(',')?[colorIndex];
        //            int sizeIndex = Array.IndexOf(codes, "Size");
        //            string selectedsize = "";
        //            if(sizeIndex!=-1)
        //            selectedsize = model.Values.Split(',')?[sizeIndex];
        //            if (viewModel.ConfigurableData.SwatchImages == null)
        //                viewModel.ConfigurableData.SwatchImages = new List<SwatchImageViewModel>();

        //            List<string> _colors = viewModel.AssociatedProducts.Select(o => o.OMSColorValue).Distinct().ToList();

        //            foreach (string color in _colors)
        //            {

        //                SwatchImageViewModel svm = new SwatchImageViewModel();
        //                svm.AttributeCode = "Color";
        //                svm.AttributeValues = color;
        //                svm.ImagePath = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorPath;
        //                viewModel.ConfigurableData.SwatchImages.Add(svm);

        //                SwatchImageViewModel colorsize = new SwatchImageViewModel();
        //                colorsize.AttributeCode = "ColorSize";
        //                List<object> colorspecificsizes = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == color).Select(o => o.Custom1).Distinct().ToList();
        //                colorsize.AttributeValues = color + "-";
        //                colorsize.Custom1 = color;
        //                colorspecificsizes.ForEach(cs => { colorsize.AttributeValues = colorsize.AttributeValues + "," + cs.ToString(); });

        //                List<object> selctedcolorsizespecificwidths = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == selectedcolor && Convert.ToString(x.Custom1) == selectedsize).Select(o => o.Custom2).Distinct().ToList();
        //                selctedcolorsizespecificwidths.ForEach(cs => { colorsize.Custom2 = colorsize.Custom2 + "," + cs.ToString(); });

        //                viewModel.ConfigurableData.SwatchImages.Add(colorsize);
        //            }
        //            //TO BE UNCOMMENTED FOR PICKUP/SHIP
        //            viewModel = SetDefaultStoreAddressDetails(viewModel);
        //        }

        //        return viewModel;
        //    }
        //    catch(Exception ex)
        //    {
        //        ZnodeLogging.LogMessage("RSProductAgent-Error-" + ex.Message + "- Stack Trace - " + ex.StackTrace, ZnodeLogging.Components.API.ToString(), TraceLevel.Error);
        //        return new ProductViewModel();
        //    }
        //}

        //public override ProductViewModel GetProduct(int productID)
        //{           
        //    ProductViewModel viewModel = base.GetProduct(productID);
        //     if (viewModel != null)
        //    {
        //        string size = viewModel.Attributes.FirstOrDefault(x => x.AttributeCode == "Size").AttributeValues;
        //        string colorval = viewModel.Attributes.FirstOrDefault(x => x.AttributeCode == "Color").SelectedAttributeValue[0];

        //        if (viewModel?.ConfigurableData?.SwatchImages == null)
        //            viewModel.ConfigurableData.SwatchImages = new List<SwatchImageViewModel>();

        //        List<string> _colors = viewModel.AssociatedProducts.Select(o => o.OMSColorValue).Distinct().ToList();
        //        List<object> _colorSizeWidthList = viewModel.AssociatedProducts.Select(o => o.Custom3).Distinct().ToList();
        //        foreach (string color in _colors)
        //        {                   
        //            SwatchImageViewModel svm = new SwatchImageViewModel();
        //            svm.AttributeCode = "Color";
        //            svm.AttributeValues = color;
        //            svm.ImagePath = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorPath;
        //            viewModel.ConfigurableData.SwatchImages.Add(svm);

        //            SwatchImageViewModel colorsize = new SwatchImageViewModel();
        //            colorsize.AttributeCode = "ColorSize";
        //            List<object> colorspecificsizes = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == color).Select(o => o.Custom1).Distinct().ToList();
        //            colorsize.AttributeValues = color + "-";
        //            colorsize.Custom1 = color;                   
        //            colorspecificsizes.ForEach(cs => { colorsize.AttributeValues = colorsize.AttributeValues + "," + cs.ToString(); });
        //            List<object> selctedcolorsizespecificwidths = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == colorval && Convert.ToString(x.Custom1)==size).Select(o => o.Custom2).Distinct().ToList();
        //            selctedcolorsizespecificwidths.ForEach(cs => { colorsize.Custom2 = colorsize.Custom2 + "," + cs.ToString(); });
        //            viewModel.ConfigurableData.SwatchImages.Add(colorsize);
        //        }
        //        //TO BE UNCOMMENTED FOR PICKUP/SHIP
        //        viewModel = SetDefaultStoreAddressDetails(viewModel);

        //        viewModel.ShowAddToCart = true;
        //        viewModel.InventoryMessage = "";
        //        viewModel.Custom4 = _colorSizeWidthList;
        //        viewModel.Custom5 = "FROMPLP";
        //    }            
        //    return viewModel;
        //}

        public override ProductViewModel GetConfigurableProduct(ParameterProductModel model)
        {
            try
            {
                ProductViewModel viewModel = base.GetConfigurableProduct(model);
                if (viewModel != null)
                {
                    string[] codes = model.Codes.Split(',');
                    int colorIndex = Array.IndexOf(codes, "Color");
                    string selectedcolor = model.Values.Split(',')?[colorIndex];
                    int sizeIndex = Array.IndexOf(codes, "Size");
                    string selectedsize = "";
                    if (sizeIndex > 0)
                        selectedsize = model.Values.Split(',')?[sizeIndex];
                    if (viewModel.ConfigurableData.SwatchImages == null)
                        viewModel.ConfigurableData.SwatchImages = new List<SwatchImageViewModel>();

                    List<string> _colors = viewModel.AssociatedProducts.Select(o => o.OMSColorValue).Distinct().ToList();
                    List<object> _colorSizeWidthList = viewModel.AssociatedProducts.Select(o => o.Custom3).Distinct().ToList();
                  
                    foreach (string color in _colors)
                    {

                        SwatchImageViewModel svm = new SwatchImageViewModel();
                        svm.AttributeCode = "Color";
                        svm.AttributeValues = color;
                        svm.ImagePath = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorPath;
                        /*Custom3 - GalleryImages*/
                        svm.Custom3 = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorCode;

                        viewModel.ConfigurableData.SwatchImages.Add(svm);

                        SwatchImageViewModel colorsize = new SwatchImageViewModel();
                        colorsize.AttributeCode = "ColorSize";
                        List<object> colorspecificsizes = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == color).Select(o => o.Custom1).Distinct().ToList();
                        colorsize.AttributeValues = color + "-";
                        colorsize.Custom1 = color;
                        colorspecificsizes.ForEach(cs => { colorsize.AttributeValues = colorsize.AttributeValues + "," + cs.ToString(); });

                        List<object> selctedcolorsizespecificwidths = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == selectedcolor && Convert.ToString(x.Custom1) == selectedsize).Select(o => o.Custom2).Distinct().ToList();
                        selctedcolorsizespecificwidths.ForEach(cs => { colorsize.Custom2 = colorsize.Custom2 + "," + cs.ToString(); });

                        viewModel.ConfigurableData.SwatchImages.Add(colorsize);
                    }
                    //TO BE UNCOMMENTED FOR PICKUP/SHIP
                    viewModel = SetDefaultStoreAddressDetails(viewModel);
                }

                return viewModel;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("RSProductAgent-Error-" + ex.Message + "- Stack Trace - " + ex.StackTrace, ZnodeLogging.Components.API.ToString(), TraceLevel.Error);
                return new ProductViewModel();
            }
        }

        public override ProductViewModel GetProduct(int productID)
        {
            ProductViewModel viewModel = base.GetProduct(productID);
            if (viewModel != null)
            {
                //string size = viewModel.Attributes.FirstOrDefault(x => x.AttributeCode == "Size").SelectedAttributeValue[0];
                string size = viewModel.Attributes.FirstOrDefault(x => x.AttributeCode == "Size").AttributeValues;
                string colorval = viewModel.Attributes.FirstOrDefault(x => x.AttributeCode == "Color").SelectedAttributeValue[0];

                if (viewModel?.ConfigurableData?.SwatchImages == null)
                    viewModel.ConfigurableData.SwatchImages = new List<SwatchImageViewModel>();

                List<string> _colors = viewModel.AssociatedProducts.Select(o => o.OMSColorValue).Distinct().ToList();
                List<object> _colorSizeWidthList = viewModel.AssociatedProducts.Select(o => o.Custom3).Distinct().ToList();
                _colorSizeWidthList.Add(',');
                foreach (string color in _colors)
                {
                    SwatchImageViewModel svm = new SwatchImageViewModel();
                    svm.AttributeCode = "Color";
                    svm.AttributeValues = color;
                    svm.ImagePath = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorPath;
                    /*Custom3 - GalleryImages*/
                    svm.Custom2 = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorSwatchText;
                    svm.Custom3 = viewModel.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorCode;
                    viewModel.ConfigurableData.SwatchImages.Add(svm);

                    SwatchImageViewModel colorsize = new SwatchImageViewModel();
                    colorsize.AttributeCode = "ColorSize";
                    List<object> colorspecificsizes = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == color).Select(o => o.Custom1).Distinct().ToList();
                    colorsize.AttributeValues = color + "-";
                    colorsize.Custom1 = color;
                    colorspecificsizes.ForEach(cs => { colorsize.AttributeValues = colorsize.AttributeValues + "," + cs.ToString(); });
                    colorsize.AttributeValues = colorsize.AttributeValues + ",";
                    List<object> selctedcolorsizespecificwidths = viewModel.AssociatedProducts.Where(x => x.OMSColorValue == colorval && Convert.ToString(x.Custom1) == size).Select(o => o.Custom2).Distinct().ToList();
                    selctedcolorsizespecificwidths.ForEach(cs => { colorsize.Custom2 = colorsize.Custom2 + "," + cs.ToString(); });
                    colorsize.Custom2 = colorsize.Custom2 + ",";
                    viewModel.ConfigurableData.SwatchImages.Add(colorsize);
                }
                //TO BE UNCOMMENTED FOR PICKUP/SHIP
                viewModel = SetDefaultStoreAddressDetails(viewModel);
                viewModel.ShowAddToCart = true;
                viewModel.InventoryMessage = "";
                viewModel.Custom4 = _colorSizeWidthList;
                viewModel.Custom5 = "FROMPLP";
            }
            return viewModel;
        }
        private ProductViewModel SetDefaultStoreAddressDetails(ProductViewModel viewModel)
        {
            try
            {
                string defaultStore = GetFromCookie("DefaultSelectedStore");
                string[] storeValues = defaultStore.Split('*');
                if (storeValues.Length == 3)
                {
                    viewModel.Custom1 = storeValues[0];
                    viewModel.Custom2 = storeValues[1];
                    viewModel.Custom3 = storeValues[2];
                }
            }
            catch(Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, "RSProductAgent", TraceLevel.Error, ex);
            }
            return viewModel;
        }

        private ShortProductViewModel SetDefaultStoreAddressDetails(ShortProductViewModel viewModel)
        {
            try
            {
                string defaultStore = GetFromCookie("DefaultSelectedStore");
                string[] storeValues = defaultStore.Split('*');
                if (storeValues.Length == 3)
                {
                    viewModel.Custom1 = storeValues[0];
                    viewModel.Custom2 = storeValues[1];
                    viewModel.Custom3 = storeValues[2];
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, "RSProductAgent", TraceLevel.Error, ex);
            }
            return viewModel;
        }

       

        public override ProductReviewViewModel GetProductForReview(int productID, string productName, decimal? rating)
        {
            _productClient.SetProfileIdExplicitly(RSWebstoreHelper.GetProfileId().GetValueOrDefault());
            PublishProductModel model = _productClient.GetPublishProduct(productID, GetRequiredFilters(), new ExpandCollection { ExpandKeys.SEO });
            if (HelperUtility.IsNotNull(model))
            {
                /*nivi code start: check null condition*/
                if(GetFromSession<List<RecentViewModel>>(ZnodeConstant.RecentViewProduct)!=null)
                { 
               // Updated rating added againest that product.
                List<RecentViewModel> recentViewProductCookie = GetFromSession<List<RecentViewModel>>(ZnodeConstant.RecentViewProduct);
                foreach (RecentViewModel recentViewModel in recentViewProductCookie.Where(x => x.PublishProductId == productID))
                {
                    recentViewModel.Rating = Convert.ToDecimal(rating);
                }
                SaveInSession(ZnodeConstant.RecentViewProduct, recentViewProductCookie);
                }
                /*nivi code end*/
                ProductReviewViewModel viewModel = model.ToViewModel<ProductReviewViewModel>();
                viewModel.PublishProductId = model.ConfigurableProductId > 0 ? model.ConfigurableProductId : model.PublishProductId;
                return viewModel;
            }
            else
                return new ProductReviewViewModel { PublishProductId = productID, ProductName = productName };
        }



    }
}
