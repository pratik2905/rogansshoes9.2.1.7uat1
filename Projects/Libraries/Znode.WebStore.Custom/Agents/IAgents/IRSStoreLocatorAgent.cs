﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore.Agents;

namespace Znode.WebStore.Custom.Agents.IAgents
{
  public interface IRSStoreLocatorAgent:IStoreLocatorAgent
    {
       void SaveInCookie(string storeId, string storeName, string storeAddress);
    }
}
