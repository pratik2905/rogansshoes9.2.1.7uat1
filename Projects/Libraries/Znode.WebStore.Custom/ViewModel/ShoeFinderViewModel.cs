﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore;
using Znode.Libraries.Resources;

namespace Znode.WebStore.Custom.ViewModel
{
    public class ShoesFinderViewModel:BaseViewModel
    {
        public string SelectBrand { get; set; }
        public string ShoeName { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public string Gender { get; set; }       
        public string width { get; set; }
        public string location { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
        
        public string Type { get; set; }
        public string StockNumber { get; set; }
        public string LikeOurSite { get; set; }
        public string Description { get; set; }
        //[RegularExpression("^[a-zA-Z0-9'' ']+$", ErrorMessageResourceName = "ErrorSpecialCharatersNotAllowed", ErrorMessageResourceType = typeof(WebStore_Resources))]
    }
}
