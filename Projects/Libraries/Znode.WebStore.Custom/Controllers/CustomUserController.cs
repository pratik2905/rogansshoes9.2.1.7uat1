﻿using System;
using System.IO;
using System.Web.Mvc;
using System.Xml.Serialization;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.SAML;
using Znode.Libraries.SAMLWeb;
using Znode.Libraries.SAMLWeb.Controllers;
using Znode.WebStore.Core.Agents;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.Engine.WebStore.Controllers
{
    public class CustomUserController : UserController
    {
        #region Private Read-only members
        private readonly IAuthenticationHelper _authenticationHelper;
        //private readonly IUserAgent _userAgent;
        private readonly IRSUserAgent _userAgent;
        private readonly ICartAgent _cartAgent;
        #endregion

        #region Public Constructor     
        public CustomUserController(IRSUserAgent userAgent, ICartAgent cartAgent, IAuthenticationHelper authenticationHelper, IPaymentAgent paymentAgent, IImportAgent importAgent, IFormBuilderAgent formBuilderAgent)
       : base(userAgent, cartAgent, authenticationHelper, paymentAgent, importAgent, formBuilderAgent)
        {
            _authenticationHelper = authenticationHelper;
            _userAgent = userAgent;
            _cartAgent = cartAgent;
        }
        #endregion

       
        //SAML call back 
        public ActionResult SAMLCallback(string returnUrl = "")
        {
            SessionObj sessionObj = SessionHelper.GetDataFromSession<SessionObj>(Request.Params["state"]);

            string samlResponseXML = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(sessionObj.SAMLToken));

            XmlSerializer serializer = new XmlSerializer(typeof(ResponseType));

            ResponseType response = (ResponseType)serializer.Deserialize(new StringReader(samlResponseXML));

            string username = string.Empty;
            //Find assertion
            AssertionType assertion = null;
            foreach (var item in response.Items)
            {
                if (item is AssertionType)
                {
                    assertion = item as AssertionType;
                }
            }

            //Find the username
            if (assertion != null)
            {
                foreach (var item in assertion.Subject.Items)
                {
                    if (item is NameIDType)
                    {

                        if (!string.IsNullOrEmpty(((NameIDType)item).Value))
                        {
                            username = ((NameIDType)item).Value;
                        }
                    }
                }
            }
            string id = Request.Params["id"];

            //Set the Authentication Cookie.  
            SetAuthCookie(username, id);

            return RedirectToAction<HomeController>(x => x.Index());
        }

        private void SetAuthCookie(string userName, string id)
        {
            Session.Add(WebStoreConstants.UserAccountKey, new UserViewModel { UserName = userName, UserId = Convert.ToInt32(id), RoleName = "admin" });

            //Set the Authentication Cookie.           
            _authenticationHelper.SetAuthCookie(userName, true);
        }

        [AllowAnonymous]
        public ContentResult MyStore(string loginUserName)
        {
            string defaultStore = _userAgent.GetDefaultStoreDetailsFromCookie();
            string storeId = string.Empty, storeName = string.Empty, storeAddress = string.Empty;
            string[] storeValues = defaultStore.Split('*');
            if (storeValues.Length > 1)
            {
                storeId = storeValues[0];
                storeName = storeValues[1];
                storeAddress = storeValues[2];
            }
            if (storeName == "")
                storeName = "Locate A Store";
            WebstoreHelper helper = new WebstoreHelper();
            UserViewModel currentUser = helper.GetUserViewModelFromSession();
            if (currentUser != null && currentUser.Custom1!=null && Convert.ToString(currentUser.Custom1) == storeId)
            {
                return Content(currentUser.Custom2);
            }
            else
                return Content(storeName);
        }
    }
}