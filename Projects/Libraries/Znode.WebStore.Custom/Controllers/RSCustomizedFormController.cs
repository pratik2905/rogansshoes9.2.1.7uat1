﻿using System.Web.Mvc;
using Znode.Engine.WebStore.Controllers;
using Znode.WebStore.Custom.Agents.Agents;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Controllers
{

    public class RSCustomizedFormController : BaseController
    {
        private readonly IRSCustomizedFormAgent _RSCustomizedFormAgent;

        public RSCustomizedFormController()
        {
          _RSCustomizedFormAgent = new RSCustomizedFormAgent();
        }
        [HttpGet]
        public virtual ActionResult ScheduleATruck()
        {
            return View("../RSCustomizedForms/ScheduleATruck");
        }
        [HttpPost]
        public virtual ActionResult ScheduleATruck(ScheduleATruckViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool flag = _RSCustomizedFormAgent.ScheduleATruck(model);

                if (flag == true)
                    return View("../RSCustomizedForms/RSCustomizedSuccessForm");
                else
                {
                    SetNotificationMessage(GetErrorNotificationMessage(model.ErrorMessage));
                }
            }
            return View(model);
            
        }
        [HttpGet]
        public virtual ActionResult ShoeFinder()
        {
           
            return View("../RSCustomizedForms/ShoeFinder");
        }
        [HttpPost]
        public virtual ActionResult ShoeFinder(ShoesFinderViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool flag = _RSCustomizedFormAgent.ShoeFinder(model);
                if (flag == true)
                    //SetNotificationMessage(GetSuccessNotificationMessage(model.SuccessMessage));
                    return View("../RSCustomizedForms/RSCustomizedSuccessForm");
                else
                {
                    SetNotificationMessage(GetErrorNotificationMessage(model.ErrorMessage));
                }
            }
            return View(model);
        }
    }
}