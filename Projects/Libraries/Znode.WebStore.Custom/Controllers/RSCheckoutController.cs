﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.WebStore.Custom.Helper;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.WebStore.Custom.Controllers
{
    public class RSCheckoutController : CheckoutController
    {
        private readonly ICheckoutAgent _checkoutAgent;

        public RSCheckoutController(IUserAgent userAgent, ICheckoutAgent checkoutAgent, ICartAgent cartAgent, IPaymentAgent paymentAgent) : base(userAgent, checkoutAgent, cartAgent, paymentAgent)
        {
            _checkoutAgent = checkoutAgent;
        }


        //Account address
        public override ActionResult AccountAddress(int userid = 0, int quoteId = 0, int addressId = 0, string addressType = "")
        {
            return PartialView("_BillingShippingAddress", _checkoutAgent.GetBillingShippingAddress(userid, quoteId > 0, addressType, addressId, 0, false));
        }
    }
}
