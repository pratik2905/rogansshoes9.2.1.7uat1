﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Sample.Api.Model.Request
{
    public class RSOrderRequestModel
    {
        //Nivi Custom Code : Start
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string OrderStatus { get; set; }
        //Nivi Custom Code : End
    }
}
